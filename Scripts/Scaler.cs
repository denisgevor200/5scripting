using UnityEngine;

public class Scaler : MonoBehaviour
{
    [SerializeField] private float _speed;

    private void Update()
    {
        float scaleSpeed = _speed * Time.deltaTime;
        transform.localScale += new Vector3(scaleSpeed, scaleSpeed, scaleSpeed);
    }
}
